from django.urls import path
from todos.views import delete_list, edit_item, edit_todo_list, list_of_todolists, new_item, show_todolist, create_list

urlpatterns = [
    path('',list_of_todolists,name="todo_list_list"),
    path('<int:id>/',show_todolist,name='todo_list_detail'),
    path('create/', create_list, name="todo_list_create"),
    path('<int:id>/edit/',edit_todo_list,name="todo_list_update"),
    path('<int:id>/delete/',delete_list,name="todo_list_delete"),
    path('items/create/',new_item,name="todo_item_create"),
    path('<int:id>/items/create/',new_item,name="create_item_from_list"),
    path('items/<int:id>/edit/',edit_item,name="todo_item_update"),
]
