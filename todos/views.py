from webbrowser import get
from django.shortcuts import redirect, render,get_object_or_404
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm,TodoItemForm


# Create your views here.


def list_of_todolists(request):
    context = {
        "todolists" : TodoList.objects.all(),
    }
    return render(request,"todos/todolists.html",context)


def show_todolist(request,id):

    todolist = get_object_or_404(TodoList,id=id)
    items = todolist.items.all()

    context = {
        "todolist" : todolist,
        "items" : items,
    }
    return render(request,'todos/detail.html',context)

def create_list(request):

    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            form = form.save()
            return redirect('todo_list_detail', form.id)
    else:
        form = TodoListForm()

    context = {
        'form': form,
    }
    return render(request, 'todos/create.html',context)


def edit_todo_list(request,id):

    todolist = get_object_or_404(TodoList, id=id)

    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            form.save()
            return redirect('todo_list_detail', id)
    else:
        form = TodoListForm(instance=todolist)

    context = {
        'form': form,
    }
    return render(request,'todos/update.html',context)



def delete_list(request,id):
    todolist =  get_object_or_404(TodoList,id=id)
    if request.method =="POST":
        todolist.delete()
        return redirect('todo_list_list')

    return render(request,'todos/delete.html')





def new_item(request,id=None):

    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            form = form.save()
            return redirect('todo_list_detail', form.list.id)
    else:
        form = TodoItemForm(initial = {'list' : id})
    context = {
        'form': form,
        'button_name' : 'Create'
    }
    return render(request,'todos/new_item.html',context)





def edit_item(request,id):
    item = get_object_or_404(TodoItem,id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST,instance=item)
        if form.is_valid():
            form = form.save()
            return redirect('todo_list_detail', form.list.id)
    else:
        form = TodoItemForm(instance=item)
    context = {
        'form': form,
        'button_name' : 'Update'
    }
    return render(request,'todos/new_item.html',context)
